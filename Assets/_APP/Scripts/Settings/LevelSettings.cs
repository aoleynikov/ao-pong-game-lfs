﻿using UnityEngine;

namespace ao.PongGame
{
    [CreateAssetMenu(fileName = "LevelSettings", menuName = "Create Settings/LevelSettings", order = 1)]
    public class LevelSettings : ScriptableObject
    {
        [Tooltip("Name of scene")] 
        public string scenePath;
        
        [Tooltip("Maximum score in single player mode")]
        public int maxSingleScore = 20;
        [Tooltip("Maximum score in multi player mode")]
        public int maxMultiScore = 10;
        [Tooltip("Maximum balls")]
        public int ballCount = 1;
        [Tooltip("Ratio of ball force")]
        public float ballForceRatio = 1;
    }
}