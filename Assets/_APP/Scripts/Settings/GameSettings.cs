﻿using System.Collections.Generic;
using UnityEngine;

namespace ao.PongGame
{
	[CreateAssetMenu(fileName = "GameSettings", menuName = "Create Settings/GameSettings", order = 1)]
	public class GameSettings : ScriptableObject
	{
		[Tooltip("Game Menu Prefab")]
		public GameMenu gameMenu;

		[Tooltip("Levels list")]
		public List<LevelSettings> levels;

		[Tooltip("Paddles for different modes: singleplayer")]
		public Paddle singlePaddle;
		[Tooltip("Paddles for different modes: host")]
		public Paddle hostPaddle;
		[Tooltip("Paddles for different modes: client")]
		public Paddle clientPaddle;

		[Tooltip("List of balls prefabs")]
		public List<Ball> balls;
	}
}