﻿using UnityEngine;

namespace ao.PongGame
{
    public class UIStatsSystem
    {
        private GameMenu _gameMenu;
        private ScoreMenu _leftScore;
        private ScoreMenu _rightScore;

        public void Initialize(GameMenu gameMenu)
        {
            _gameMenu = gameMenu;
        }

        /// <summary>
        /// Setup stats panel
        /// </summary>
        public void SetupStatsPanel(NetworkGameType networkGameType)
        {
            switch (networkGameType)
            {
                case NetworkGameType.Single:

                    _leftScore = GameObject.Instantiate(_gameMenu.scoreLeft);
                    _leftScore.transform.SetParent(_gameMenu.statsPanels.transform);

                    ResetScore(_leftScore);
                    break;

                case NetworkGameType.Host:
                case NetworkGameType.Client:

                    _leftScore = GameObject.Instantiate(_gameMenu.scoreLeft);
                    _leftScore.transform.SetParent(_gameMenu.statsPanels.transform);

                    _rightScore = GameObject.Instantiate(_gameMenu.scoreRight);
                    _rightScore.transform.SetParent(_gameMenu.statsPanels.transform);

                    ResetScore(_leftScore);
                    ResetScore(_rightScore);
                    break;
            }
        }

        public void ResetScore(ScoreMenu scoreMenu)
        {
            scoreMenu.score.text = "0";
        }

        public void SetLeftScore(int val, int maxScore)
        {
            _leftScore.score.text = "" + val + " / " + maxScore;
        }

        public void SetRightScore(int val, int maxScore)
        {
            _rightScore.score.text = "" + val + " / " + maxScore;
        }
    }
}
