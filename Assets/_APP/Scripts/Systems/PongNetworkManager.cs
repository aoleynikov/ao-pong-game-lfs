﻿using System.Net;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.Networking;

namespace ao.PongGame
{
	public class PongNetworkManager : NetworkManager
	{
		private GameSettings _gameSettings;

		private bool _isSingle = false;
		private bool _isHost = false;
		private bool _isSession = false;

		private Camera _camera;
		private ScoreByNetwork _scoreByNetwork;

		// setup game objects and dependencies
		public void Initialize(GameSettings gameSettings, Camera mainCamera)
		{
			_gameSettings = gameSettings;
			_camera = mainCamera;

			GameEvents.OnSingleStarted += RunSingle;
			GameEvents.OnHostStarted += RunHost;
			GameEvents.OnClientStarted += RunClient;

			GameEvents.OnShowHostIP.Invoke(GetHostIP());
		}

		// close connection, unsubscribe events
		public void OnDisable()
		{
			if (_isHost)
			{
				StopHost();
			}
			else
			{
				StopClient();
			}

			GameEvents.OnSingleStarted -= RunSingle;
			GameEvents.OnHostStarted -= RunHost;
			GameEvents.OnClientStarted -= RunClient;
		}

		// setup different players for different network modes
		public override void OnClientConnect(NetworkConnection conn)
		{
			ClientScene.Ready(conn);

			if (_isSingle)
			{
				playerPrefab = _gameSettings.singlePaddle.gameObject;
				ClientScene.AddPlayer(0);
				ClientScene.AddPlayer(1);
			}
			else if (_isHost)
			{
				playerPrefab = _gameSettings.hostPaddle.gameObject;
				// rotate camera for best view on host
				_camera.transform.Rotate(0, 0, 180);
				ClientScene.AddPlayer(0);
			}
			else
			{
				playerPrefab = _gameSettings.clientPaddle.gameObject;
				ClientScene.AddPlayer(1);
			}
		}

		private void RunSingle()
		{
			_isSingle = true;
			StartHost();
		}

		private void RunHost()
		{
			_isSingle = false;
			StartHost();

		}

		private void RunClient(string ip)
		{
			_isSingle = false;
			networkAddress = ip;
			StartClient();
		}

		private void Update()
		{

			if (CanStartHostGame() && !_isSession)
			{
				if (!_isSingle)
				{
					GameEvents.OnStartMultiPlayerSession.Invoke(NetworkGameType.Host);
				}
				else
				{
					GameEvents.OnStartSinglePlayerSession.Invoke(NetworkGameType.Single);
				}

				_isSession = true;
			}
		}

		private bool CanStartHostGame()
		{

			bool canStart = (NetworkServer.connections.Count == 2 && _isHost && !_isSingle) ||
			                _isSingle;

			return canStart;
		}

		public override void OnStartHost()
		{
			base.OnStartHost();
			_isHost = true;
		}

		public void RemovePlayers()
		{
			ClientScene.RemovePlayer(0);
			ClientScene.RemovePlayer(1);
		}

		private string GetHostIP()
		{
			IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
			foreach (IPAddress addr in localIPs)
			{
				if (addr.AddressFamily == AddressFamily.InterNetwork)
				{
					return addr.ToString();
				}
			}

			return "localhost";
		}
	}

}