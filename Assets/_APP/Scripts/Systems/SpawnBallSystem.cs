﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace ao.PongGame
{
	public class SpawnBallSystem
	{
		private GameSettings _gameSettings;
		private LevelSettings _levelSettings;

		private List<Ball> _ball = new List<Ball>();

		public void Initialize(GameSettings gameSettings, LevelSettings levelSettings)
		{
			_gameSettings = gameSettings;
			_levelSettings = levelSettings;
		}

		public void SpawnBall()
		{
			for (int i = 0; i < _levelSettings.ballCount; i++)
			{
				Ball ball = GameObject.Instantiate(_gameSettings.balls[Random.Range(0, _gameSettings.balls.Count)]);
				ball.Initialize(_levelSettings.ballForceRatio);

				NetworkServer.Spawn(ball.gameObject);

				_ball.Add(ball);
			}
		}

		public void UnspawnBall()
		{
			for (int i = _ball.Count - 1; i >= 0; i--)
			{
				_ball[i].DestroyObject();
				_ball.RemoveAt(i);
			}
		}
	}
}
