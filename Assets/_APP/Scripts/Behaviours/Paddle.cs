﻿using UnityEngine;
using UnityEngine.Networking;

namespace ao.PongGame
{
	public class Paddle : NetworkBehaviour
	{
		private Vector3 offset;
		private float minScreenX = -2.8f;
		private float maxScreenX = 2.8f;

		private void Start()
		{
			if (!isLocalPlayer)
			{
				return;
			}

			// events
			GameEvents.OnPaddleMove += MovePaddle;
		}

		private void OnDestroy()
		{
			GameEvents.OnPaddleMove -= MovePaddle;
		}

		private void MovePaddle(NetworkBehaviour paddle, float val)
		{
			if (this == paddle)
			{
				return;
			}

			transform.position = new Vector2(val, transform.position.y);
		}


		private void OnMouseDown()
		{
			if (!isLocalPlayer)
			{
				return;
			}

			offset = gameObject.transform.position -
			         Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10.0f));
		}

		private void OnMouseDrag()
		{
			if (!isLocalPlayer)
			{
				return;
			}

			Vector3 newPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
			Vector2 newPos = Camera.main.ScreenToWorldPoint(newPosition) + offset;
			newPos.x = Mathf.Clamp(newPos.x, minScreenX, maxScreenX);

			transform.position = new Vector2(newPos.x, transform.position.y);

			GameEvents.OnPaddleMove.Invoke(this, newPos.x);
		}
	}

}
