﻿using UnityEngine.Networking;

namespace ao.PongGame
{
	public class ScoreByNetwork : NetworkBehaviour
	{
		[SyncVar] private int bottomScore = 0;
		[SyncVar] private int upperScore = 0;

		private NetworkGameType _networkGameType = NetworkGameType.Client;

		#region INITIALIZE AND DISPOSE

		private void OnEnable()
		{

			bottomScore = 0;
			upperScore = 0;

			// events
			// game session
			GameEvents.OnStartMultiPlayerSession += StartMultiPlayerSession;
			GameEvents.OnStartSinglePlayerSession += StartSinglePlayerSession;

			// gameplay
			GameEvents.OnGoal += Goal;
		}

		private void OnDisable()
		{
			// game session
			GameEvents.OnStartMultiPlayerSession -= StartMultiPlayerSession;
			GameEvents.OnStartSinglePlayerSession -= StartSinglePlayerSession;

			// gameplay
			GameEvents.OnGoal -= Goal;
		}

		#endregion

		#region GAME SESSION

		private void StartSinglePlayerSession(NetworkGameType networkGameType)
		{
			_networkGameType = networkGameType;
		}

		private void StartMultiPlayerSession(NetworkGameType networkGameType)
		{
			_networkGameType = networkGameType;
		}

		#endregion

		#region SCORE COUNT

		private void Goal(string aimTag)
		{
			if (aimTag.Equals("Bottom"))
			{
				bottomScore++;
				CmdUpdateScore();
			}
			else if (aimTag.Equals("Upper"))
			{
				upperScore++;
				CmdUpdateScore();
			}
		}

		[Command]
		void CmdUpdateScore()
		{
			RpcTellAllClientsToUpdateScoreBottom(bottomScore, upperScore);
		}

		[ClientRpc]
		void RpcTellAllClientsToUpdateScoreBottom(int bottomScore, int upperScore)
		{
			GameEvents.OnScoreChanged.Invoke(bottomScore, upperScore);
		}

		#endregion
	}
}
